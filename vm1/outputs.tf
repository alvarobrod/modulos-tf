output "public_ip" {
  value = azurerm_public_ip.mypip.ip_address
}

output "subnet_id" {
  value = azurerm_subnet.mysubnet.id
}
