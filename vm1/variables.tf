variable "resource_group_name" {
  default = "myrsg"
}

variable "resource_group_location" {
  default = "westeurope"
}

variable "virtual_network_name" {
  default = "default-network"
}

variable "address_space" {

}

variable "subnet_name" {
  default = "default-subnet"
}

variable "address_prefix" {

}

variable "pip_name" {
  default = "default-pip"
}

variable "pip_allocation_method" {
  default = "Dynamic"
}

variable "network_interface_name" {
  default = "default-nic"
}

variable "ip_conf_name" {
  default = "default-ip-conf"
}

variable "address_allocation" {
  default = "Static"
}

variable "private_ip_address" {

}

variable "virtual_machine_name" {

}

variable "vm_size" {
  default = "Standard_B2s"
}

variable "delete_os_disk" {
  type    = bool
  default = true
}

variable "delete_data_disks" {
  type    = bool
  default = true
}

variable "image_publisher" {
  default = "credativ"
}

variable "image_offer" {
  default = "Debian"
}

variable "image_sku" {
  default = "9"
}

variable "image_version" {
  default = "latest"
}

variable "os_disk_name" {
  default = "default-os-disk"
}

variable "os_disk_caching" {
  default = "ReadWrite"
}

variable "os_disk_create_option" {
  default = "FromImage"
}

variable "os_disk_managed_disk_type" {
  default = "Standard_LRS"
}

variable "admin_username" {
  default = "alvaro"
}

variable "admin_password" {
  default = "P4$$w.rd"
}

variable "disable_password_auth" {
  type    = bool
  default = true
}
