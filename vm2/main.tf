resource "azurerm_public_ip" "mypip" {
  name                = "${var.pip_name}${var.virtual_machine_name}"
  resource_group_name = var.resource_group_name
  location            = var.resource_group_location
  allocation_method   = var.pip_allocation_method
}

resource "azurerm_network_interface" "mynic" {
  name                = "${var.network_interface_name}${var.virtual_machine_name}"
  resource_group_name = var.resource_group_name
  location            = var.resource_group_location

  ip_configuration {
    name                          = "${var.ip_conf_name}${var.virtual_machine_name}"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = var.address_allocation
    private_ip_address            = var.private_ip_address
    public_ip_address_id          = azurerm_public_ip.mypip.id
  }
}

resource "azurerm_virtual_machine" "myvm" {
  name                  = var.virtual_machine_name
  resource_group_name   = var.resource_group_name
  location              = var.resource_group_location
  network_interface_ids = [azurerm_network_interface.mynic.id]
  vm_size               = var.vm_size

  delete_os_disk_on_termination    = var.delete_os_disk
  delete_data_disks_on_termination = var.delete_data_disks

  storage_image_reference {
    publisher = var.image_publisher
    offer     = var.image_offer
    sku       = var.image_sku
    version   = var.image_version
  }

  storage_os_disk {
    name              = "${var.os_disk_name}${var.virtual_machine_name}"
    caching           = var.os_disk_caching
    create_option     = var.os_disk_create_option
    managed_disk_type = var.os_disk_managed_disk_type
  }

  os_profile {
    computer_name  = var.virtual_machine_name
    admin_username = var.admin_username
    admin_password = var.admin_password
  }

  os_profile_linux_config {
    disable_password_authentication = var.disable_password_auth
    ssh_keys {
      path     = "/home/${var.admin_username}/.ssh/authorized_keys"
      key_data = "${file("~/.ssh/id_rsa.pub")}"
    }
  }
}
